<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MangaCategories extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//if the mangaList table exists, lets create the table that will house the categories for each manga
		Schema::create('manga_categories', function( Blueprint $t ) {
			$t->increments('id')->unsigned();
			$t->string('category');
			$t->timestamps();
		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('manga_categories');

	}

}
