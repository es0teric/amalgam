<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsernameCol extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//since sentry doesnt give us a "username" column, 
		//we will create one for ourselves on the same table
		if( Schema::hasTable('users') ) {

			Schema::table('users', function($t) {
				$t->string('username');
			});

		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if( Schema::hasTable('users') ) {

			Schema::table('users', function($t) {
				$t->dropColumn('username');
			});

		}
	}

}
