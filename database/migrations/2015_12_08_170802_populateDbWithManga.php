<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateDbWithManga extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::create('manga_list', function( Blueprint $t ) {
			$t->increments('id');
			$t->string('title');
			$t->string('alias')->nullable();
			$t->integer('hits');
			$t->string('manga_id');
			$t->string('manga_img')->nullable();
			$t->integer('last_chapter_date')->nullable();
			$t->string('status'); //1 = ongoing, 2 = completed
			$t->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('manga_list');
	}

}