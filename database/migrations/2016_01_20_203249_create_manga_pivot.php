<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMangaPivot extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('category_manga', function( Blueprint $t ) {
			$t->integer('manga_list_id')->unsigned();
			$t->foreign('manga_list_id')->references('id')->on('manga_list');
			$t->integer('manga_categories_id')->unsigned();
			$t->foreign('manga_categories_id')->references('id')->on('manga_categories');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('category_manga');
	}

}
