<?php


use Illuminate\Database\Seeder;

class SentinelUserSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		if(Schema::hasTable('users'))
		{

			//seed table with ghost's info
			Sentinel::registerAndActivate(array(
				'email' => 'ghost@amalgam.dev',
				'password' => 'ghost123',
				'username' => 'es0teric',
				'first_name' => 'Leon',
				'last_name' => 'F',
				'permissions' => array(
					'user.create' => 1,
					'user.delete' => 1,
					'user.view' => 1,
					'user.update' => 1
				)
			));

			//seed table with kyuu's user info
			/*Sentinel::registerAndActivate(array(
				'email' => 'kyuu@amalgam.dev',
				'password' => 'apu123',
				'username' => 'kyuu',
				'first_name' => 'Apu',
				'last_name' => 'S',
				'permissions' => array(
					'user.create' => 1,
					'user.delete' => 1,
					'user.view' => 1,
					'user.update' => 1
				)
			));*/

			//display success info
			$this->command->info('User table seeded!');

		}
		else
		{
			//user table doesnt exist
			$this->command->error('Please migrate sentry2 first!');
		}
	}

}
