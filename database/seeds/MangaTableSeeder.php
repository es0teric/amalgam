<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;
use Jenssegers\Mongodb\Connection;
use Jenssegers\Mongodb\Schema\Blueprint;

class MangaTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		//MangaData::collection()->push();
		var_dump(MangaData::first());

		if(MangaData::first() == null)
		{

			//initialize guzzle in IoC container to use
			$guzzle = App::make('\GuzzleHttp\Client');

			//get json from initial api query and output all manga array items
			$manga = $guzzle->get('https://www.mangaeden.com/api/list/0')->json()['manga'];

			$collection = new MangaData;

			//var_dump($collection);
			$collection->manga = $manga;
			
			$collection->save();
			
			//print_r($manga);
		
			//open bracket for progresss
			//echo "[";
			//first we loop through all manga results, because we want to add to the current array
			/*foreach($manga as $i => $chap) 
			{
				//TODO: readd $chap['ld'] into array by looping through it again... 
				//since it gives undefined index errors

				//var_dump(Carbon::parse($chap['ld']));

				//then we use the "i" param which is the ID of the manga in the api to retrieve
				//more detailed chapter info
				$m = $guzzle->get("https://www.mangaeden.com/api/manga/{$chap['i']}")->json();

				//add params that are needed for manga chapters
				$chap['g'] = $m['categories'];
				$chap['d'] = $m['description'];
				$chap['h'] = $m['hits'];

				$data[] = $chap;

				//var_dump($chap['ld']);

				try 
				{
					//then seed into mongodb
					$collection 		= new MangaData;
					$collection['i'] 	= $chap['i'];
					$collection['a'] 	= $chap['a'];
					$collection['h'] 	= $chap['h'];
					$collection['im'] 	= $chap['im'];
					//$collection['ld'] 	= $chap['ld'];
					$collection['s'] 	= $chap['s'];
					$collection['t']	= $chap['t'];
					$collection['g']	= $chap['g'];
					$collection['d']	= $chap['d'];

					//TODO: make sure this is updated to only modify "updated_at"
					//after initial insert
					$collection['created_at'] = Carbon::now();
					$collection['updated_at'] = Carbon::now();
					//$collection->save();
				}
				catch(Exception $e)
				{
					throw new Exception('Something went wrong...', 0, $e);
				}

				//each = sign is a item in the loop
				//echo "=";

			}*/

			//print_r($manga);

			//close bracket
			//echo "] 100% COMPLETE!";

			$this->command->info('Mongo Seeded!');

			/*echo "<pre>";
					//var_dump($chap["ld"]);
					//print_r($chap);
					//print_r($m);
				echo "</pre>"; */
		}

	}

}