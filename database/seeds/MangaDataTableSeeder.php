<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;
use Jenssegers\Mongodb\Connection;
use Jenssegers\Mongodb\Schema\Blueprint;

class MangaDataTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		$mData = MangaData::first()->manga;

		//var_dump($mData);

		foreach($mData as $i => $manga)
		{
			//var_dump($manga);
			$data = new Manga;
			
			$data->alias 	= $manga['a'];
			$data->hits 	= $manga['h'];

			if(array_key_exists('h', $manga))
			{
				$data->hits = $manga['h'];
			}

			$data->id 		= $manga['i'];
			$data->img 		= $manga['im'];

			if(array_key_exists('ld', $manga))
			{
				$data->lastDate = $manga['ld'];
			}
			
			$data->status 	= $manga['s'];
			$data->title 	= $manga['t'];

			var_dump($data);
			$data->save();

		}

		$this->command->info('100% Complete!');

		//var_dump($mData);
	}

}