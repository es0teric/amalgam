<?php

class Manga extends Moloquent {

	/**
	 * Used to set table (in mongodb, it is a collection)
	 * 
	 * @access protected
	 * @var string
	 **/
	protected $collection = "manga";

	/**
	 * Used to set mongodb connection
	 * 
	 * @access protected
	 * @var string
	 **/
	protected $connection = "mongodb";

	protected $fillable = [
		'alias',
		'hits',
		'id',
		'img',
		'lastDate',
		'status',
		'title'
	];

	public $timestamps = false;
	
}