<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bookmarks extends Model {

    protected $table = "bookmarks";
    protected $fillable = [ 'user_id', 'manga_list_id' ];

    public function users() {
        return $this->belongsTo('App\Models\User');
    }

    public function manga() {
        return $this->hasMany('App\Models\Manga');
    }

}
