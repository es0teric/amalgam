<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MangaCategories extends Model {

	protected $table = 'manga_categories';
	protected $fillable = ['category'];
	protected $guarded = ['id'];

	public function mangaList() {
		return $this->belongsToMany('App\Models\MangaList', 'manga_pivot');
	}

}
