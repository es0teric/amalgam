<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model {

	protected $table = 'manga_categories';
	protected $fillable = ['category'];

	public function mangas() {
		return $this->belongsToMany('App\Models\Manga', 'category_manga', 'manga_categories_id', 'manga_list_id');
	}

}