<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MangaList extends Model {

	protected $table = 'manga_list';
	protected $fillable = ['title', 'alias', 'hits', 'mangaId', 'lastChapterDate', 'mangaImg', 'status', 'created_at', 'updated_at'];
	protected $guarded = ['id'];

	public function mangaCategories() {
		return $this->belongsToMany('App\Models\MangaCategories', 'manga_pivot');
	}

}
