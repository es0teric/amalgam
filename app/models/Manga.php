<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client as Guzzle;
use Response;
use Carbon\Carbon;
use Categories;

class Manga extends Model {

	protected $table = 'manga_list';

	protected $fillable = ['title', 'alias', 'hits', 'manga_id', 'last_chapter_date', 'manga_img', 'status', 'created_at', 'updated_at'];

	/**
	 * Establishes relation between categories pivot table
	 *
	 * @return
	 */
	public function categories() {
		return $this->belongsToMany('App\Models\Categories', 'category_manga', 'manga_list_id', 'manga_categories_id');
	}

	/**
	 * Gets singular manga data from mangaeden api based off manga id
	 *
	 * @param  string $mangaId manga id from db
	 * @return string          JSON string response of data pulled from API
	 */
	protected function getSingleManga( $mangaId ) {

		$client = new Guzzle;
		$req = $client->createRequest('GET', 'https://www.mangaeden.com/api/manga/' . $mangaId );
		$res = $client->send($req);

		/*$lastChapDate = $res->json()['last_chapter_date'];
		var_dump();
		$res->json()['last_chapter_date'] = Carbon::createFromTimestamp($lastChapDate)->format('F j, Y');

		dd($res->json());*/

		return $res->json();

	}

	/**
	 * Searches manga in db based off title
	 * @param  string  $title manga title to search in db
	 * @param  boolean $json  wether to output results in json format or not
	 * @return array|string   returns either a PHP array or a json object in string form
	 */
	protected function searchManga( $title, $json = false ) {

		$items = $this->where('alias', 'like', '%' . $title . '%')->paginate(8, ['manga_id']);

		//might want to use !$items->isEmpty()
		if( !$items->isEmpty() ) {

			foreach( $items as $item ) {

				$singleManga = $this->getSingleManga( $item->manga_id );

				/**
				 * @todo check if aka contains japanese characters in keys 0,1
				 */

				$mangaList[] = [
					"manga_api_id" => $item->manga_id,
					"alias" => $singleManga['alias'],
					"title" => $singleManga['title'],
					"author" => $singleManga['author'],
					"categories" => $singleManga['categories'],
					"chapter_count" => $singleManga['chapters_len'],
					"description" => $singleManga['description'],
					"img" => $singleManga['image'],
					"year_released" => $singleManga['released'],
					"status" => $singleManga['status'],
					"chapter_list" => $singleManga['chapters']
				];

				//checks if imageURL exists in the $singleManga return array
				if( array_key_exists( 'imageURL', $singleManga ) )
					$mangaList['imgUrl'] = $singleManga['imageURL'];

				if( isset( $singleManga['aka'][0] ) && strlen( $singleManga['aka'][0] ) != strlen( utf8_decode( $singleManga['aka'][0] ) ) )
					$mangaList['aka'] = $singleManga['aka'][0];

				elseif( isset( $singleManga['aka'][1] ) && strlen( $singleManga['aka'][1] ) != strlen( utf8_decode( $singleManga['aka'][1] ) ) )
					$mangaList['aka'] = $singleManga['aka'][1];

				//print_r( Manga::getSingleManga( $item->manga_id ) );

			}

			$data = [
				'paginator' => [
					'total' => $items->total(),
					'lastPage' => $items->lastPage(),
					'perPage' => $items->perPage(),
					'currentPage' => $items->currentPage(),
					'path' => $items->url('search'),
					'paginatorRender' => $items->appends(['s' => $title])->render()
				],
				'data' => $mangaList
			];

			if( $json )
				return Response::json( $data );
			else
				return $data;

		} else {

			//dont return anything yet

		}

	}

}

//$book->categories()->associate($category) or ->sync([])
//$book->categories()->save($category, [... extra pivot data ...])
