<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Sentinel;
use Response;

class VerifyCsrfToken extends BaseVerifier {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

		// If request is an ajax request, then check to see if token matches token provider in
    	// the header. This way, we can use CSRF protection in ajax requests also.
    	$token = $request->ajax() ? $request->header('X-CSRF-Token') : $request->input('_token');
        
		//return true;
		//return $request->session()->token() == $token;

		/*if( !is_null( $token ) )
    		return $request->session()->token() == $token;
		else
			return parent::handle($request, $next);*/

		return parent::handle($request, $next);
	}

}
