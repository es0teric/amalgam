<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::get('/', 'WelcomeController@index');
Route::get('home', 'HomeController@index');
/*Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);*/

Route::get('login', [ 'as' => 'login', 'uses' => 'PagesController@showLogin'] );
Route::post('login', [ 'uses' => 'PagesController@doLogin'] );
Route::get('logout', [ 'as' => 'logout', 'uses' => 'PagesController@doLogout'] );
Route::get('dashboard', [ 'before' => 'auth', 'uses' => 'DashboardController@showDashboard'] );

Route::get('search/{query?}', [ 'before' => 'auth', 'uses' => 'SearchController@index'] );
//Route::post('bookmark/{id}', ['before' => ['auth', 'csrf'], 'uses' => 'SingleMangaController@bookmarkManga']);
Route::resource('bookmark', 'BookmarksController', ['only' => ['index', 'store', 'show', 'destroy']]);
//Route::post('search', [ 'before'=> 'csrf', 'uses' => 'SearchController@getResults' ]);

Route::get('manga/{id?}', [ 'before' => 'csrf', 'uses' => 'SingleMangaController@index' ]);

//Route::get('api', ['uses' => 'ApiController@index'] );
Route::get('api/v1/list/{count?}', ['uses' => 'ApiController@index', 'middleware' => 'cors']);
