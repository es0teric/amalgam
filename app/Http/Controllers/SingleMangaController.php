<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Manga;
use App\Models\Bookmarks;
use GuzzleHttp\Client as Guzzle;
use View, Response, Sentinel;

class SingleMangaController extends Controller {

    /**
     * Displays single manga view
     *
     * @return \Illuminate\View\View
     */
    public function index( $title ) {

        $manga = Manga::where('alias', '=', $title)->first();
        $bookmark = Bookmarks::where( 'manga_list_id', '=', $manga->id );

        if( $bookmark->exists() ) {
            $items['bookmarked'] = true;
        } else {
            $items['bookmarked'] = false;
        }

        $items['id'] = $manga->id;

        $data = array_merge( $items, Manga::getSingleManga( $manga->manga_id ) );

        View::share( 'title', 'Manga &raquo; ' . $manga->title );
        return View::make('single')->with('data', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
