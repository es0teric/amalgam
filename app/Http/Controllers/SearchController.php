<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Manga;
use App\Models\Categories;
use Illuminate\Http\Request;
use App\Helpers as Helper;
use View, Input, Response, Redirect;

class SearchController extends Controller {

	/**
	 * Invoked on page init
	 */
	public function __construct()
	{
		View::share('title', 'User &raquo; Search');
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Illuminate\View
	 */
	public function index() {

		//Manga::searchCategories(14832);

		if( !empty( Input::get('q') ) ) {

			echo "Test";

			$searchQuery = filter_var( Input::get('q'), FILTER_SANITIZE_STRIPPED );
			$match = preg_match( '/\s/', $searchQuery );

			if( $match == 1 ) {
				$q = preg_replace( '/\s/', '-', $searchQuery );
				$items = Manga::searchManga( $q );
			} else {
				$items = Manga::searchManga( $searchQuery );
			}

			return View::make('search')->with('results', $items);

		} elseif( !empty( Input::get('c') ) ) {

			$categoryQuery = filter_var( Input::get('c'), FILTER_SANITIZE_STRIPPED );
			$items['cat'] = $categoryQuery;

			$items['data'] = Manga::whereHas('categories', function($q) use( $categoryQuery ) {
				$q->where('category', '=', $categoryQuery);
			})->paginate(10);


			return View::make('search')->with('results', $items)->render();

		} else {
			return View::make('search')->render();
		}

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
