<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use Sentinel;
use Redirect;
use App\Models\Bookmarks;
use App\Models\Manga;

class DashboardController extends Controller {

	protected $layout = 'layouts.public.global';

	/**
	 * Invoked on page init
	 */
	public function __construct()
	{
		View::share('title', 'User &raquo; Dashboard');
	}

	/**
	 * Shows dashboard view
	 * @return void
	 */
	public function showDashboard()
	{
		//return dashboard view
		if( Sentinel::check() ) {

			$bookmarks = Bookmarks::where('user_id', '=', Sentinel::getUser()->id)->lists('manga_list_id')->all();
			$bookmarkedManga = Manga::whereIn('id', $bookmarks)->get();

			$data = [
				'bookmarks' => $bookmarkedManga
			];

			//dd(Manga::all());
			//Event::fire('manga.load');
			//return View::make('dashboard');

			return view('dashboard')->with('results', $data);
				//->with( 'data', MangaData::first()->manga );
		} else {
			Redirect::to('login')->with('message', 'Login failed, man!');
		}
	}

}
