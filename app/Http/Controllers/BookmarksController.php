<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Bookmarks;
use Sentinel, Input, Response;

class BookmarksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request ) {

        if( $request->isMethod('post') ) {

            $id = filter_var( Input::get('id'), FILTER_SANITIZE_NUMBER_INT );
            $items = Bookmarks::where('manga_list_id', '=', $id);
            $user = Sentinel::getUser();

            //lets check if there are any existing bookmarks with the same ID
            if( !$items->exists() ) {

                $bookmark = Bookmarks::create(['user_id' => $user->id, 'manga_list_id' => $id]);

                if( !empty( $bookmark ) ) {
                    return Response::json([
                        'code' => 200,
                        'message' => 'Bookmark saved!'
                    ]);
                } else {
                    return Response::json([
                        'error' => 500,
                        'message' => 'Error adding bookmark, please check token or mysql query.'
                    ]);
                }
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bookmark = Bookmarks::where('manga_list_id', '=', $id)->first();
        dd($bookmark);
        //$bookmark->manga()->attach($id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        Bookmarks::where('manga_list_id', '=', $id)->delete();

        $bookmark = Bookmarks::where('manga_list_id', '=', $id)->get();

        if( $bookmark->isEmpty() ) {
            return Response::json([
                'code' => 200,
                'message' => 'Bookmark removed!'
            ]);
        } else {
            return Response::json([
                'code' => 500,
                'message' => 'Error removing the bookmark, please check SQl'
            ]);
        }

    }
}
