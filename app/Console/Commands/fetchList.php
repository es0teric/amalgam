<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use GuzzleHttp, Storage;

class fetchList extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'fetchlist';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Fetches manga list from external api.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$client = new GuzzleHttp\Client([
			'base_url' => 'https://www.mangaeden.com/api/list/0'
		]);

		//works: https://www.mangaeden.com/api/list/0?p=0&l=30

		$req = $client->get();
		$statusCode = $req->getStatusCode();

		if ($statusCode >= 200 && $statusCode < 300) {

			$json = $req->json();
			$contents = json_encode( $json['manga'], JSON_PRETTY_PRINT );
			
			//$this->info($contents);
			//$this->info( 'Amount of objects returned: ' . count( json_decode( $contents ) ) );

			//apend to file: Storage::append('file.txt', $content)

			if( Storage::disk('local')->put('list.json', $contents ) ) {
				$this->info('List created!');
			}
		}

		//https://www.mangaeden.com/api/list/0/
		//https://www.mangaeden.com/api/list/[language]/?p=X 
		//$this->info('successfully ran!');
	}


}
