<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class initApp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'startEngine';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initializes fetchList & populateMangaTable';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('fetchlist');
        $this->call('populateMangaTable');
    }
}
