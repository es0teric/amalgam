<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use \App\Models\Manga;
use \App\Models\Categories;
use Storage, Model, Carbon\Carbon;

class populateMangaTable extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'populateMangaTable';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Populates manga table with latest data from list.json file';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{

		$decodeList = json_decode( Storage::disk('local')->get('list.json') );
		//$chunk = array_chunk( $decodeList, 50 );
		//$firstChunk = $chunk[0];

		$carbon = new Carbon;

		$this->info('Initiating manga list import...');
		echo "\r";
		$bar = $this->output->createProgressBar( count( $decodeList) );

		//lets loop through each item in the manga eden list
		foreach( $decodeList as $item ) {

			$manga = new Manga;
			$mangaList = $manga::where( 'alias', '=', $item->a )->first();

			//lets check to see if a title already exists in the db
			if( empty( $mangaList ) ) {

				$manga->title = $item->t;
				$manga->hits = $item->h;
				$manga->alias = $item->a;

				//manga id from API
				$manga->manga_id = $item->i;

				//lets check if image exists
				if( empty( $item->im ) )
					$manga->manga_img = null;
				else
					$manga->manga_img = $item->im;

				//lets check the status of the current manga
				if( $item->s == 1 )
					$manga->status = 'ongoing';

				if( $item->s == 2 )
					$manga->status = 'completed';

				if( property_exists( $item , 'ld' ) )
					//unix timestamp for last updated chapter
					$manga->last_chapter_date = $item->ld;
				else
					$manga->last_chapter_date = null;

				$manga->created_at = date('Y-m-d H:i:s');

				$manga->save();

				if( !empty( $item->c ) ) {

					foreach( $item->c as $category ) {

						$mangaCategories = new Categories;

						//lets check if the current category exists in the list so we can go ahead and populate that ID into the pivot table
						$cat = $mangaCategories::where('category', '=', $category )->first();

						if( empty( $cat ) ) {

							/*echo "\n\r";
							print_r( 'manga id: ' . $manga->id );
							echo "\n\r";
							print_r( 'manga title: ' . $item->t );
							echo "\n\r";*/

							//$mangaCategories::create([ 'category' => $category ]);
							$mangaCategories->category = $category;

							$mangaCategories->save();

							/*echo "\n\r";
							print_r( 'manga category id: ' . $mangaCategories->id );
							echo "\n\r";
							print_r( 'manga category: ' . $category );
							echo "\n\r";*/


							$mangaCategories->mangas()->attach( $manga->id );
							//$manga->categories()->attach( $mangaCategories->id );

						} else {
							$cat->mangas()->attach( $manga->id );
						}

					}

				}

				$bar->advance();

				//$this->info('saved: ' . $item->t );

			} else { }

		}

		echo "\r";
		$bar->finish();
		echo "\r";
		$this->info('Manga list imported successfully!');
		echo "\r";

		exit;

		//$this->info('command initiated!');
	}

}
