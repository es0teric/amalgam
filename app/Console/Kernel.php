<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
		'App\Console\Commands\fetchList',
		'App\Console\Commands\populateMangaTable',
		'App\Console\Commands\initApp'
		//'App\Console\Commands\dbCheckCommand'
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->command('inspire')
				 ->hourly();

		$schedule->command('fetchList')
				 ->twiceDaily(8, 18);

		$schedule->command('populateMangaTable')
				 ->twiceDaily(8, 18);
	}

}
