var elixir = require('laravel-elixir'),
	gulp = require('gulp'),
	sass = require('gulp-sass'),
	gutil = require('gulp-util'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	livereload = require('gulp-livereload'),
	notify = require('gulp-notify'),
	bower = require('gulp-bower'),
	minifycss = require('gulp-minify-css'),
	watch = require('gulp-watch'),
	browserSync = require('browser-sync').create();

var config = {
     sassPathRoot: './resources/assets/sass',
	sassPathPartials: './resources/assets/source/frontend/partials',
	sassFrontend: './resources/assets/source/frontend',
     bowerDir: './bower',
	bourbonDir: './bower/bourbon/app/assets/stylesheets',
    bittersDir: './bower/bitters/app/assets/stylesheets',
    neatDir: './bower/neat/app/assets/stylesheets',
	jsDir: './resources/assets/js'
}

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

gulp.task('css', function() {

	return gulp.src( config.sassPathRoot + '/app.scss' )
         .pipe( sass({
             style: 'compressed',
             loadPath: [
                 './resources/sass',
                 config.bowerDir + '/fontawesome/scss',
				config.bourbonDir + '_bourbon.scss',
				config.bittersDir + '_base.scss',
				config.neatDir
             ]
         }) 
        .on("error", notify.onError(function (error) {
          	return "Error: " + error.message;
          }))) 
		.pipe( minifycss() )
		.pipe( rename({
			suffix: '.min'
		}))
         .pipe( gulp.dest( './public/assets/build/css' ) )
		.pipe( notify({ message: 'CSS compiled successfully!' }) ); 

});

gulp.task('icons', function() {
	return gulp.src( config.bowerDir + '/components-font-awesome/fonts/**.*' )
		.pipe( gulp.dest( './public/assets/build/fonts' ) );
});

gulp.task('bower', function() { 
    return bower()
         .pipe( gulp.dest( config.bowerDir ) ) 
});

gulp.task( 'compile-js', function() {
	return gulp.src( config.jsDir + "/*.js")
		.pipe( uglify().on('error', gutil.log ) )
		.pipe( rename({suffix: '.min'}) )
		.pipe( gulp.dest('./public/assets/build/js') )
		.pipe( notify({ message: "Js compiled and minified!" }) )
});

gulp.task('init', ['css', 'icons', 'bower', 'compile-js'], function() {

	//initialize browserSync
	browserSync.init({
      proxy: "http://amalgam.dev/",
		files: [
			config.sassPathRoot + '/*.scss',
			config.sassPathPartials + '/*.scss',
			config.sassFrontend + '/*.scss',
			{
				match: [
					'./resources/views/layouts/public/*.php',
					'./resources/views/partials/*.php',
					'./resources/views/*.php',
					'./resources/views/errors/*.php',
					'./resources/views/emails/*.php'
				],
				fn: function( event, file ) {
					this.reload();
				}
			}
		]
	});

	gulp.watch( config.jsDir + '/*.js', ['compile-js'] )
		.on( 'change', function() {
			browserSync.reload();
			notify( 'Js compiled!' ).write('');
		});

	gulp.watch( config.sassPathRoot + '/*.scss', ['css'] )
		.on('change', function() {
			browserSync.reload();
			notify('CSS compressed from root scss & browser reloaded!').write('');
		});

	gulp.watch( config.sassPathPartials + '/*.scss', ['css'] )
		.on('change', function() {
			browserSync.reload();
			notify('CSS compressed from frontend/partials & browser reloaded!').write('');
		});

	gulp.watch( config.sassFrontend + '/*.scss', ['css'] )
		.on('change', function() {
			browserSync.reload();
			notify('CSS compressed from frontend folder & browser reloaded!').write('');
		});

});
