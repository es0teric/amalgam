$(document).ready(function() {

    size_li = $("#chapter_list li").size();

    x = 5;

    $('#chapter_list li:lt('+x+')').show();

    $('#loadMore').click(function () {
        x = ( x+5 <= size_li ) ? x+5 : size_li;
        $('#chapter_list li:lt('+x+')').show();
    });

    $('#showLess').click(function () {
        x = ( x-5 < 0 ) ? 3 : x-5;
        $('#chapter_list li').not(':lt('+x+')').hide();
    });

    var bookmarkIcon = $('#manga-bookmark').find('.fa-bookmark-o'),
        token = $('meta[name="_token"]').attr('content'),
        $this = $(this);

    console.log(token);

    if( bookmarkIcon.length !== 0 ) {

        //adds bookmark
        $('#manga-bookmark').hover(function() {
            $(this).find('i').removeClass('fa-bookmark-o').addClass('fa-bookmark');
        }, function() {
            $(this).find('i').removeClass('fa-bookmark').addClass('fa-bookmark-o');
        });

        $('#manga-bookmark').on('click', function() {

            $.ajax({
                type: 'POST',
                url: '/bookmark',
                data: {
                    _token: token,
                    id: $(this).data('id')
                },
                headers: {
                    'X-CSRF-TOKEN': token
                },
                error: function(data) {
                    console.log("---------------------ERROR-------------------");
                    console.log(data);
                }
            }).done(function(data) {
                console.log(data.code);
                if( data.code == 200 )
                    $this.find('i').removeClass('fa-bookmark-o').addClass('fa-bookmark');
            });

        });

    } else {

        //removes bookmark
        $('#manga-bookmark').hover(function() {
            $(this).find('i').removeClass('fa-bookmark').addClass('fa-bookmark-o');
        }, function() {
            $(this).find('i').removeClass('fa-bookmark-o').addClass('fa-bookmark');
        });

        $('#manga-bookmark').on('click', function() {

            $.ajax({
                type: 'DELETE',
                url: '/bookmark/' + $(this).data('id'),
                data: {
                    _token: token
                },
                headers: {
                    'X-CSRF-TOKEN': token
                },
                error: function(data) {
                    console.log("---------------------ERROR-------------------");
                    console.log(data);
                }
            }).done(function(data) {

                console.log( $(this).find('.fa-bookmark').removeClass('fa-bookmark') );
                /*
                    @todo figure out why the next line does not immediately change the bookmark icon after click on both delete and post method
                */
                if( data.code == 200 )
                    $(this).find('i').removeClass('fa-bookmark').addClass('fa-bookmark-o');

            });

            //console.log('removing bookmark clicked!');
        });
    }

    console.log($('#chapter_list li').length);

    $(".dropodown-menu-and-button").click(function() {
        
        console.log("Test");
        //$('.dropdown').fadeToggle();

        var $button, $menu;
        
        $button = $(this);
        //$menu = $button.siblings(".dropdown-menu");
        $menu = $button.closest('.dropodown-menu-and-button').find('.dropdown-menu');
        console.log($menu);
        
        $menu.toggleClass("show-menu");
        /*$menu.children("li").click(function() {
          $menu.removeClass("show-menu");
          $button.html($(this).html());
        });*/ 

    });

});
