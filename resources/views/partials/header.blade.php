<header class="navigation">
  <div class="navigation-wrapper">
    <a href="javascript:void(0)" class="logo">
      <img src="https://raw.githubusercontent.com/thoughtbot/refills/master/source/images/placeholder_square.png" alt="">
    </a>
    <a href="" class="navigation-menu-button" id="js-mobile-menu">MENU</a>
    <div class="nav">
      <ul id="navigation-menu">
        <li class="nav-link"><a href="/dashboard">Dashboard</a></li>
        <li class="nav-link"><a href="/search">Search</a></li>
        <li class="nav-link"><a href="javascript:void(0)">Blog</a></li>

      </ul>
    </div>
    <div class="navigation-tools">
      <ul>
        <li>
          <a href="#">Hi, {!! Sentinel::getUser()->first_name !!}</a>
        </li>
        <li class="dropodown-menu-and-button">
            <a href="#" class="dropdown-button"><i class="fa fa-bars fa-lg"></i></a>
            <div class="dropdown">
                <div class="dropdown-container">
                    <ul class="dropdown-menu dropdown-select">
                        <li>Profile</li>
                        <li>Messages</li>
                        <li>Settings</li>
                        <li>{!! link_to_route('logout', 'Logout') !!}</li>
                    </ul>
                </div>
            </div> <!-- /.dropdown -->
        </li>
      </ul>
    </div> <!-- .navigation-tools -->
  </div>

</header>
