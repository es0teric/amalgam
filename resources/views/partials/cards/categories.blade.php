
<?php
    //lets query the manga eden api for the data we need
    $client = new \GuzzleHttp\Client();
    $req = $client->createRequest('GET', 'https://www.mangaeden.com/api/manga/' . $result->manga_id );
    $res = $client->send($req);
    $resData = $res->json();
?>

<div class="card">
    <div class="card-image">
        @if( isset( $result->manga_img ) )
        <img src="https://cdn.mangaeden.com/mangasimg/{!! $result->manga_img !!}" width="200" />
    @elseif( isset( $resData['imgURL'] ) )
        <img src="{!! $resData['imgURL'] !!}" width="200" />
        @endif
    </div> <!-- /.card-image -->

    <div class="card-header">

        <a href="manga/{!! $result->alias !!}">
            @if( isset( $result->title ) )
            {!! $result->title !!}
            @endif
        </a>

    </div> <!-- /.card-header -->

    <div class="card-copy">
        @if( isset( $resData['description'] ) )
            @if( trim( $resData['description'] ) == 'N/A' )
                No description available.
            @else
            {!! Str::words( $resData['description'], 50, '...') !!}
            @endif
        @endif
    </div> <!-- /.card-copy -->

</div> <!-- /.card -->
