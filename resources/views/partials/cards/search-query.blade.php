@if( isset( $result['alias'] ) )

<div class="card">
    <div class="card-image">
        @if( isset( $result['img'] ) )
        <img src="https://cdn.mangaeden.com/mangasimg/{!! $result['img'] !!}" width="200" />
        @elseif( isset( $result['imgUrl'] ) )
        <img src="{!! $result['imgUrl'] !!}" width="200" />
        @endif
    </div> <!-- /.card-image -->

    <div class="card-header">

        <a href="manga/{!! $result['alias'] !!}">
            @if( isset( $result['title'] ) )
            {!! $result['title'] !!}
            @endif
        </a>

    </div> <!-- /.card-header -->

    <div class="card-copy">
        @if( isset( $result['description'] ) )
            @if( trim( $result['description'] ) == 'N/A' )
                No description available.
            @else
            {!! Str::words( $result['description'], 50, '...') !!}
            @endif
        @endif
    </div> <!-- /.card-copy -->

</div> <!-- /.card -->
@endif
