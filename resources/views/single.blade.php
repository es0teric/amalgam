@extends('layouts.public.global')

@section('content')

    <div id="single-container">
      <div id="sidebar">
        <p>
            @if( !$data['bookmarked'] )
            <span id="manga-bookmark" data-id="{!! $data['id'] !!}"><i class="fa fa-bookmark-o fa-2x" aria-hidden="true"></i></span>
            @else
            <span id="manga-bookmark" data-id="{!! $data['id'] !!}"><i class="fa fa-bookmark fa-2x" aria-hidden="true"></i></span>
            @endif

            <img src="https://cdn.mangaeden.com/mangasimg/{!! $data['image'] !!}" />
        </p>
        <h3>{!! $data['title'] !!} ({!! $data['released'] !!})</h3>
        <p class="data_aka">
          @if( isset( $data['aka'][0] ) )
            {!! $data['aka'][0] !!}
          @elseif( isset( $data['aka'][1] ) )
            {!! $data['aka'][1] !!}
          @endif

          <!--
            TODO: check if $data['aka'] keys 0 and 1 are set and are japanese characters
          -->
        </p>
        <p> {!! $data['artist'] !!} / {!! $data['author'] !!} </p>
        <ul class="category_list">
            @foreach( $data['categories'] as $category )
                <li><a href="/search?c={{ strtolower( preg_replace( '/\s/', '-', $category ) ) }}">{{ $category }}</a></li>
            @endforeach
        </ul>
        <!--<p>
          {!! implode(' / ', $data['categories']) !!}
        </p>-->
      </div> <!-- ./#sidebar -->

      <div id="content-container">
        @if( trim( $data['description'] ) == 'N/A' )
        <p> No description available. </p>
        @else
        <p> {!! $data['description'] !!} </p>
        @endif
        <p> {!! $data['chapters_len'] !!} Chapters </p>
        <ul id="chapter_list">
            @foreach( $data['chapters'] as $chapter )
              <li>
                <span data-chapter-id="{!! $chapter[3] !!}">
                  @if( is_numeric( $chapter[2] ) )
                    Chapter {{ $chapter[2] }}
                  @elseif( empty( $chapter[2] ) )
                    Chapter {{ $chapter[0] }}
                  @else
                    {{ $chapter[0] }} // {{ $chapter[2] }}
                  @endif
                </span>
              </li>
            @endforeach
        </ul>

        <div id="loadMore">Load more</div>
        <div id="showLess">Show less</div>
      </div> <!-- ./#content-container -->
    </div> <!-- ./#single-container -->

    <!--{!! var_dump( Carbon::createFromTimestamp( $data['last_chapter_date'] )->format( 'F j, Y' ) ) !!}

    <pre>
      {!! var_dump($data) !!}
    </pre>-->
@stop
