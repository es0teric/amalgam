@extends('layouts.public.global')

@section('content')
<h2></h2>

	<?php //var_dump(Sentinel::getUser()); ?>
	<!--<pre>
		@foreach( $results['bookmarks'] as $bookmark )
			<img src="https://cdn.mangaeden.com/mangasimg/{{ $bookmark->manga_img }}" width="150" height="200" />
		@endforeach
	</pre>-->

    <ul id="bookmark-list">
        @foreach( $results['bookmarks'] as $bookmark )
			<li><span class="manga-bookmark"><i class="fa fa-bookmark fa-2x" aria-hidden="true"></i></span><a href="/manga/{{ $bookmark->alias }}"><img src="https://cdn.mangaeden.com/mangasimg/{{ $bookmark->manga_img }}" width="150" height="200" /></a></li>
		@endforeach
    </ul>

	<!--
		<div class="cards">
		  <div class="card">
		    <div class="card-image">
		      <img src="https://raw.githubusercontent.com/thoughtbot/refills/master/source/images/mountains.png" alt="">
		    </div>
		    <div class="card-header">
		      First Card
		    </div>
		    <div class="card-copy">
		      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, officiis sunt neque facilis culpa molestiae necessitatibus delectus veniam provident.</p>
		    </div>
		    <div class="card-stats">
		      <ul>
		        <li>98<span>Items</span></li>
		        <li>298<span>Things</span></li>
		        <li>923<span>Objects</span></li>
		      </ul>
		    </div>
		  </div>
		  </div>
	-->
@stop
