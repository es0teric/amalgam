@extends('layouts.public.global')

@section('content')

	{!! Form::open(['url' => 'search', 'method' => 'GET', 'class' => 'form-search search-bar', 'role' => 'search']) !!}

		{!! Form::text( 'search', null, ['placeholder' => 'enter search term', 'name' => 'q'] ) !!}

		<button type="submit" class="searchSubmit">
			<i class="fa fa-search fa-lg" aria-hidden="true"></i>
		</button>

	{!! Form::close() !!}

	@if( isset( $results ) )

	<div class="cards">

		@if( isset( $results['cat'] ) )
			@foreach( $results['data'] as $result )
				@include('partials.cards.categories')
			@endforeach
		@else
			@foreach( $results['data'] as $result )
				@include('partials.cards.search-query')
			@endforeach
		@endif

	</div> <!-- /.cards -->

		@if( array_key_exists('paginator', $results) )
	    	{!! $results['paginator']['paginatorRender'] !!}
		@else
			{!! $results['data']->appends(['c' => $results['cat']])->render() !!}
		@endif

	@endif
@stop
