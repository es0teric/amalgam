<html lang="en">
	<head>
		<meta charset="utf-8" />
	  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	  	<meta name="description" content="Amalgam, point blank">
	  	<meta name="author" content="G. Leo">
		<meta name="_token" content="{{ csrf_token() }}" />
		<title>{!! $title !!}</title>
		<link rel="stylesheet" type="text/css" href="/assets/build/css/app.min.css">
	</head>

	<body>

		{!! View::make('partials.header') !!}

		<div id="dash-container">
			 @yield('content')
		</div>

		{!! View::make('partials.footer') !!}

	</body>

</html>
